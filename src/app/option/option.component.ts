import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Option} from '../interfaces'
import {NgIf, NgOptimizedImage} from "@angular/common";
@Component({
  selector: 'app-option',
  standalone: true,
  imports: [
    NgOptimizedImage,
    NgIf
  ],
  templateUrl: './option.component.html',
  styleUrl: './option.component.css'
})
export class OptionComponent {
@Input() option!:Option | undefined;
@Output() chooseCard=new EventEmitter<number>()

constructor() {
}

}
