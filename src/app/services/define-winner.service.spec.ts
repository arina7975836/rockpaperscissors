import { TestBed } from '@angular/core/testing';

import { DefineWinnerService } from './define-winner.service';

describe('DefineWinnerService', () => {
  let service: DefineWinnerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DefineWinnerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
