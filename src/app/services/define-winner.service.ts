import { Injectable } from '@angular/core';
import {log} from "@angular-devkit/build-angular/src/builders/ssr-dev-server";

@Injectable({
  providedIn: 'root'
})
export class DefineWinnerService {
  //ключ Map-это строка из id-шников выбранного варианта ботом/юзером или наоборот;значение-получение балла или нет за 1 игру
  allCombinations:Map<string,number>;
  constructor() {
    this.allCombinations=new Map([
      ['12',1],
      ['13',0],
      ['11',1],
      ['21',0],
      ['22',1],
      ['23',1],
      ['31',1],
      ['32',0],
      ['33',1],
    ])
  }

  getScore(firstOptionId:number,secondOptionId:number){
    let key=`${firstOptionId}`+`${secondOptionId}`

    return this.allCombinations.get(key)
  }
}
