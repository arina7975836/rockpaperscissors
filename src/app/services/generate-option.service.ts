import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GenerateOptionService {

  constructor() { }

  generateOption(arr:Array<number>){
    return arr[Math.floor(Math.random() * arr.length)]
  }
}
