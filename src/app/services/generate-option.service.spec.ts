import { TestBed } from '@angular/core/testing';

import { GenerateOptionService } from './generate-option.service';

describe('GenerateOptionService', () => {
  let service: GenerateOptionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GenerateOptionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
