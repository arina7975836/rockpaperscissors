import {Component, inject, ViewChild} from '@angular/core';
import {OptionComponent} from "../option/option.component";
import {Option, Player} from '../interfaces'
import {NgForOf} from "@angular/common";
import {GenerateOptionService} from "../services/generate-option.service";
import {ChosenOptionComponent} from "../chosen-option/chosen-option.component";
import {DefineWinnerService} from "../services/define-winner.service";
import {WinnerModalComponent} from "../winner-modal/winner-modal.component";



@Component({
  selector: 'app-game-field',
  standalone: true,
  imports: [
    OptionComponent,
    NgForOf,
    ChosenOptionComponent,
    WinnerModalComponent,
  ],
  providers:[GenerateOptionService],
  templateUrl: './game-field.component.html',
  styleUrl: './game-field.component.css'
})
export class GameFieldComponent {
  @ViewChild(WinnerModalComponent)
  private winnerComponent: WinnerModalComponent=new WinnerModalComponent();

  private generateOptionService:GenerateOptionService=inject(GenerateOptionService)
  private defineWinner:DefineWinnerService=inject(DefineWinnerService)


  options:Array<Option>

  user!:Player;
  bot!:Player;
  constructor() {
    this.bot={
      name:'bot',
      score:0,
      option:undefined
    }
    this.user={
      name:'user',
      score:0,
      option:undefined,
    }

    this.options=[
        {
          id:1,
          url:'../../assets/images/stone.png',
          name:"Камень"
        },
        {
          id:2,
          url:'../../assets/images/scissors.png',
          name:"Ножницы"
        },
        {
          id:3,
          url:'../../assets/images/paper.png',
          name:"Бумага"
        }
    ]

  }

  chooseCard(optionId:number){
    let optionsIdArr=this.options.map(item=>item.id);
    let randomOption=this.generateOptionService.generateOption(optionsIdArr)

    this.user.option=this.options.find(option=>option.id===optionId)
    this.bot.option=this.options.find(option=>option.id===randomOption)

    this.countScore()

    setTimeout(()=>{
      this.user.option=undefined;
      this.bot.option=undefined;
    },3000)
  }

  countScore(){
    let botIdOption:number=this.bot.option ? this.bot.option.id : 0;
    let userIdOption:number=this.user.option ? this.user.option.id : 0;

    // @ts-ignore
    this.bot.score+=this.defineWinner.getScore(botIdOption,userIdOption)
    // @ts-ignore
    this.user.score+=this.defineWinner.getScore(userIdOption,botIdOption)

    if(this.bot.score>4||this.user.score>4){
      if (this.bot.score<this.user.score)
      this.winnerComponent.openModal("Вы победили!")
      if (this.bot.score>this.user.score)
        this.winnerComponent.openModal("Вы проиграли...")
      if (this.bot.score==this.user.score)
        this.winnerComponent.openModal("Ничья!")
    }
  }

  startAgain(){
    this.bot.score=0;
    this.bot.option=undefined;

    this.user.score=0
    this.user.option=undefined;
  }

}
