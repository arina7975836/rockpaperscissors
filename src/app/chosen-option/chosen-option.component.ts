import {Component, Input, SimpleChanges} from '@angular/core';
import {Player} from "../interfaces";
import {OptionComponent} from "../option/option.component";
import {NgIf} from "@angular/common";

@Component({
  selector: 'app-chosen-option',
  standalone: true,
  imports: [
    OptionComponent,
    NgIf
  ],
  templateUrl: './chosen-option.component.html',
  styleUrl: './chosen-option.component.css'
})
export class ChosenOptionComponent {
@Input() initiator!:Player;

constructor() {

}

}
