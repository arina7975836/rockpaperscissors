import {Component, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {GameFieldComponent} from "./game-field/game-field.component";
import {WinnerModalComponent} from "./winner-modal/winner-modal.component";
@Component({
  selector: 'app-root',
  standalone: true,
  imports: [CommonModule, GameFieldComponent, WinnerModalComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  title = 'rock-paper-scissors';
}
