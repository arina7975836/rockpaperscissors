import {Component, ElementRef, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';


@Component({
  selector: 'app-winner-modal',
  standalone: true,
  imports: [],
  templateUrl: './winner-modal.component.html',
  styleUrl: './winner-modal.component.css'
})
export class WinnerModalComponent implements OnInit{
  @ViewChild('myModal')
  myModal!: ElementRef;

  @Output() startAgain=new EventEmitter()

  message!:string;
  constructor() { }
  ngOnInit(): void {
  }
  openModal(msg:string) {
    this.message=msg
    this.myModal.nativeElement.style.display = 'flex';
  }
  closeModal() {
    this.myModal.nativeElement.style.display = 'none';
  }
  start(){
    this.closeModal()
    this.startAgain.emit()
  }
}
