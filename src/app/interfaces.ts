export interface Option {
  id:number;
  url:string;
  name:string;
}

export interface Player{
  name:string;
  score:number;
  option:Option | undefined;
}


